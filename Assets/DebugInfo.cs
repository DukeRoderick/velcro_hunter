﻿using UnityEngine;
using System.Collections;

public class DebugInfo : MonoBehaviour {

    public UnityEngine.UI.Text DebugText = null;
    public Character PlayerCharacter = null;
    private int cachedKeyEvents = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.AppendLine("Velocity: " + PlayerCharacter.CurrentVelocity);
        sb.AppendLine("Grounded: " + PlayerCharacter.Grounded);
        if (cachedKeyEvents < PlayerCharacter.SizeOfKeyEvents)
            cachedKeyEvents = PlayerCharacter.SizeOfKeyEvents;
        sb.AppendLine("Key Events: " + cachedKeyEvents);
        DebugText.text = sb.ToString();
    }
}
