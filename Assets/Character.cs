﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Character : MonoBehaviour {

    public Vector2 CurrentVelocity { get { return characterBody.velocity; } }
    public bool Grounded { get { return grounded; } }
    public bool Stuck { get { return stuck; } }
    public int SizeOfKeyEvents { get { return KeyEventsSinceLastFixedUpdate.Count; } }
    public Animator movingAnimation = null;
    public SpriteRenderer characterSprite = null;
    public SpriteRenderer gunSprite = null;
    public Rigidbody2D characterBody = null;
    public CircleCollider2D feetCollider = null;
    public LayerMask collisionDetection = new LayerMask();
    public Transform Launcher = null;
    public Transform[] LauncherSnaps = new Transform[2];
    public Transform LaserStart = null;
    public LineRenderer LineRenderer = null;

    public float InitialVelocity = 5f;
    public float MaxVelocity = 3f;
    public float JumpVelocity = 5f;
    public float LaserExtension = .5f;

    private bool grounded = false;
    private bool stuck = false;
    private float gunRotation = 0.0f;
    private bool flipSprite = false;
    public LinkedList<KeyCode> KeyEventsSinceLastFixedUpdate = new LinkedList<KeyCode>();
    private Vector2 newMovement = new Vector2();
    //ObservableCollection
    //private List<KeyCode> pressedKeys = new List<KeyCode>();
    // Use this for initialization

    void Update()
    {
        PCInput();
        RotateGun();
        ActivateGunLine();
    }
    void FixedUpdate()
    {
        grounded = Physics2D.OverlapCircle(new Vector2(transform.position.x, transform.position.y) + feetCollider.offset, feetCollider.radius, collisionDetection) != null;
        PCMovementCalculation();
        MoveCharacter();
        UpdateSprite();
    }

    private void PCInput()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            KeyEventsSinceLastFixedUpdate.AddLast(KeyCode.Space);
        if (Input.GetKeyDown(KeyCode.D))
            KeyEventsSinceLastFixedUpdate.AddLast(KeyCode.D);
        if (Input.GetKeyDown(KeyCode.A))
            KeyEventsSinceLastFixedUpdate.AddLast(KeyCode.A);

        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A))
        {
            if (Input.GetKey(KeyCode.D) == true && characterBody.velocity.x > 0)
                KeyEventsSinceLastFixedUpdate.AddLast(KeyCode.D);
            else if (Input.GetKey(KeyCode.A) == true && characterBody.velocity.x < 0)
                KeyEventsSinceLastFixedUpdate.AddLast(KeyCode.A);
            else if (Input.GetKey(KeyCode.D) && Input.GetKeyUp(KeyCode.A))
                KeyEventsSinceLastFixedUpdate.AddLast(KeyCode.D);
            else if ((Input.GetKey(KeyCode.A) && Input.GetKeyUp(KeyCode.D)))
                KeyEventsSinceLastFixedUpdate.AddLast(KeyCode.A);
        }
        else
            KeyEventsSinceLastFixedUpdate.AddLast(KeyCode.None);

        Vector2 mousePos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        Vector2 objectPos = Camera.main.WorldToViewportPoint(Launcher.transform.position);
        gunRotation = Mathf.Atan2(mousePos.y - objectPos.y, mousePos.x - objectPos.x) * Mathf.Rad2Deg;
        flipSprite = Camera.main.ScreenToWorldPoint(Input.mousePosition).x < transform.position.x;
        //float dot = Vector2.Dot(transform.right, Launcher.transform.right);
    }

    private void PCMovementCalculation()
    {
        newMovement = new Vector2();
        LinkedListNode<KeyCode> lastInputs = KeyEventsSinceLastFixedUpdate.First;
        while(lastInputs != null)
        {
            if (lastInputs.Value == KeyCode.D)
                newMovement = new Vector2(InitialVelocity, newMovement.y);
            else if (lastInputs.Value == KeyCode.A)
                newMovement = new Vector2(-InitialVelocity, newMovement.y);
            else if (lastInputs.Value == KeyCode.Space && grounded == true)
                newMovement = new Vector2(newMovement.x, JumpVelocity);
            else
                newMovement = new Vector2(-characterBody.velocity.x * .1f, newMovement.y);
            lastInputs = lastInputs.Next;
        }
        KeyEventsSinceLastFixedUpdate.Clear();
    }

    private void UpdateSprite()
    {
        if (movingAnimation.GetBool("Jump") == false)
        {
            if (characterBody.velocity.x > 0 || characterBody.velocity.x < 0)
            {
                //characterSprite.flipX = characterBody.velocity.x < 0;
                movingAnimation.SetBool("MoveForward", true);
            }
            else
            {
                movingAnimation.SetBool("MoveForward", false);
            }
            if (characterBody.velocity.y > 0)
            {
                movingAnimation.SetBool("Jump", true);
            }

        }
        else
        {
            if (grounded == true && characterBody.velocity.y <= 0)
            {
                movingAnimation.SetBool("Jump", false);
            }
        }
        //Debug.Log("DOT: " + dot);
        if (flipSprite == true)
        {
            characterSprite.flipX = true;
            gunSprite.flipY = true;
            Launcher.transform.position = LauncherSnaps[0].position;
        }
        else
        {
            characterSprite.flipX = false;
            gunSprite.flipY = false;
            Launcher.transform.position = LauncherSnaps[1].position;
        }
    }

    private void MoveCharacter()
    {
        characterBody.AddForce(newMovement, ForceMode2D.Impulse);
        Vector2 capSpeed = characterBody.velocity;
        capSpeed.x = Mathf.Clamp(capSpeed.x, -MaxVelocity, MaxVelocity);
        characterBody.velocity = capSpeed;
    }

    public void RotateGun()
    {
        Launcher.transform.eulerAngles = new Vector3(0, 0, gunRotation);
    }

    public void ActivateGunLine()
    {
        Vector2 startPos = Vector2.zero;
        if (flipSprite == true)
            startPos = new Vector2(LaserStart.localPosition.x, -LaserStart.localPosition.y);
        else
            startPos = LaserStart.localPosition;
        Vector2 start = startPos;
        Vector2 end = new Vector2(start.x + LaserExtension, start.y);
        LineRenderer.SetPosition(0, start);
        LineRenderer.SetPosition(1, end);
    }
}

public enum Movement { Left, Right, Jump, None};
public enum KeysPressed { Left, Right, Jump, None};
